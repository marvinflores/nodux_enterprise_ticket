frappe.provide("erpnext.stock_entry");

frappe.ui.form.on("Stock Entry", {

  refresh: function(frm) {
    frm.add_custom_button(__("Generar Ticket"), function() {
      var w = window.open(
        frappe.urllib.get_full_url("/api/method/nodux_enterprise_ticket.stock_entry.creahtml_se?"
        +"name="+(frm.doc.name)
        ));
    }).addClass("btn-primary");
	}
});
