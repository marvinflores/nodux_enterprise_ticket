
# Copyright (c) 2013, NODUX and contributors
# For license information, please see license.txt
# item-costo-precio de venta-diferencia-utilidad-costo-costo-venta%-venta
from __future__ import unicode_literals
import frappe
import time
from frappe import utils
from frappe.model.document import Document
from frappe.utils import flt
from frappe import msgprint, _
import json
from frappe.utils.pdf import get_pdf

def execute(filters=None):
    columns = [("No. Factura")+"::100",("Item")+"::250","Cant.","Precio Unitario"]
    invoice_list = traer_pro(filters)
    data = []
    for inv in invoice_list:
		row = [ inv.parent,inv.item_code,inv.qty,inv.unit_price_with_tax]
		data.append(row)
    return columns, data

def get_conditions(filters):
	conditions=""
	if filters.get("name"): conditions += filters.get("name")
	return conditions

def traer_pro(filters):
	conditions = get_conditions(filters)
	cons = ""
	if conditions:
		cons = frappe.db.sql(""" SELECT parent,item_code,qty,unit_price_with_tax
						FROM `tabPurchases Invoice Item One`
                        where `tabPurchases Invoice Item One`.parent= %s""",conditions, as_dict=1)
        return cons
# crear ticket
@frappe.whitelist()
def creahtml(name1):
    if name1 != "undefined":
        html = print_ticket(name1)
        name="ticket"
        frappe.local.response.filename = "{name}.pdf".format(name=name.replace(" ", "-").replace("/", "-"))
        frappe.local.response.filecontent = get_pdf(html)
        frappe.local.response.type = "download"


def print_ticket(name1):
    product=traer_pro1(name1)
    company=frappe.db.get_value("Company",{},["name"])
    etiqueta=frappe.db.get_value("Stock Settings",{},["mostrar_en_etiqueta_1","mostrar_en_etiqueta_2","mostrar_en_etiqueta_3","mostrar_en_etiqueta_4","leyenda_etiqueta_1","leyenda_etiqueta_2","leyenda_etiqueta_3","leyenda_etiqueta_4"],as_dict=1)
    com='"';
    precio_a=com+"no_add"+com
    precio_b=com+"no_add"+com
    precio_c=com+"no_add"+com
    precio_d=com+"no_add"+com
    etiqueta_1=com+""+com
    etiqueta_2=com+""+com
    etiqueta_3=com+""+com
    etiqueta_4=com+""+com
    if etiqueta.mostrar_en_etiqueta_1 == "1":
        precio_a=com+"agregar"+com
        etiqueta_1=com+etiqueta.leyenda_etiqueta_1+com
    if etiqueta.mostrar_en_etiqueta_2 == "1":
        precio_b=com+"agregar"+com
        etiqueta_2=com+etiqueta.leyenda_etiqueta_2+com
    if etiqueta.mostrar_en_etiqueta_3 == "1":
        precio_c=com+"agregar"+com
        etiqueta_3=com+etiqueta.leyenda_etiqueta_3+com
    if etiqueta.mostrar_en_etiqueta_4 == "1":
        precio_d=com+"agregar"+com
        etiqueta_4=com+etiqueta.leyenda_etiqueta_4+com



    style="<style>p.line {line-height: 100%;font-size:11px;}</style>"
    for_t1="for (i in product){ var w=1;while (w <= product[i].qty){document.write("+com+"<p class='line'><b><font style='text-transform: uppercase;'>"+com+"+product[i].item_name.substr(0,30)+"+com+"</font></b><br> <b><font style='text-transform: uppercase;'>"+com+"+product[i].item_name.substr(31,60)+"+com+"</font></b><br> CODIGO: "+com+"+product[i].item_code+"+com+"<br>"+com+" ); if (precio_a == "+com+"agregar"+com+"){document.write(etiqueta_1+"+com+": "+com+"+product[i].precio_a+"+com+"<br>"+com+");}if (precio_b == "+com+"agregar"+com+"){document.write(etiqueta_2+"+com+": "+com+"+product[i].precio_b+"+com+"<br>"+com+");}if (precio_c== "+com+"agregar"+com+"){document.write(etiqueta_3+"+com+": "+com+"+product[i].precio_c+"+com+"<br>"+com+");}if (precio_d == "+com+"agregar"+com+"){document.write(etiqueta_4+"+com+": "+com+"+product[i].precio_d);}document.write("+com+"</br></br><p style='font-size:6px'> EMPRESA TECNOLOGICA TONERS CIA. LTDA.<img src='http://192.168.1.19:8000/files/toners.jpg' alt='' width='35' height='20' align='left'/></br></br></br></br>"+com+");w++; }}"
    # for_t1="for (i in product){ var w=1;while (w <= product[i].qty){document.write("+com+"<p class='line'>"+com+"+product[i].item_code+"+com+"<br>"+com+"+product[i].item_name+"+com+"<br><img src='http://192.168.1.19:8000/files/toners.jpg' alt='' width='60' height='25' align='left'/></br></br></br></br> </p>"+com+" ); w++; }}"
    datos_product = json.dumps(product)

    path_html ="/home/frappe/frappe-bench/apps/nodux_enterprise_ticket/nodux_enterprise_ticket/print_ticket/report/productos_compra/html.html"
    html=(open(path_html, "rb").read())
    html = html.format(datos_product=datos_product,for_t1=for_t1,style=style,precio_a=precio_a,precio_b=precio_b,precio_c=precio_c,precio_d=precio_d,etiqueta_1=etiqueta_1,etiqueta_2=etiqueta_2,etiqueta_3=etiqueta_3,etiqueta_4=etiqueta_4)
    print html
    return html

def traer_pro1(name1):
    fact_num=name1
    cons = frappe.db.sql(""" SELECT `tabItem`.item_name,`tabPurchases Invoice Item One`.parent,`tabPurchases Invoice Item One`.item_code,qty,precio_a,precio_b,precio_c,precio_d
					FROM `tabPurchases Invoice Item One`
                    LEFT JOIN `tabItem` on `tabItem`.item_code=`tabPurchases Invoice Item One`.item_code
                    where `tabPurchases Invoice Item One`.parent= %s""",fact_num,as_dict=1)
    return cons
