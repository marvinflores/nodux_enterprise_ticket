// Copyright (c) 2016, NODUX and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.provide("frappe.views");
frappe.provide("frappe.query_reports");
frappe.provide("frappe.ui.graphs");

frappe.query_reports["Productos Compra"] = {
	"filters": [

		{
			"fieldname":"name",
			"label": __("Numero Compra"),
			"fieldtype": "Link",
			"options": "Purchases Invoice One"
		}
	],
  onload: function(report) {
    report.page.add_inner_button(__("Generar Tickets"), function() {
      var filters = report.get_values();
      year=filters.fiscal_year;
      month=filters.month;
      var w = window.open(
        frappe.urllib.get_full_url("/api/method/nodux_enterprise_ticket.print_ticket.report.productos_compra.productos_compra.creahtml?"
        +"name1="+(filters.name)
        ));
    });
  }
  }
