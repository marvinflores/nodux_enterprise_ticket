# Copyright (c) 2013, ticket and contributors
# For license information, please see license.txt

# from __future__ import unicode_literals
# import frappe
#
# def execute(filters=None):
# 	columns, data = [], []
# 	return columns, data

# Copyright (c) 2013, NODUX and contributors
# For license information, please see license.txt
# item-costo-precio de venta-diferencia-utilidad-costo-costo-venta%-venta
from __future__ import unicode_literals
import frappe
import time
from frappe import utils
from frappe.model.document import Document
from frappe.utils import flt
from frappe import msgprint, _
import json
import pdfkit, os, frappe
from frappe.utils import scrub_urls
from frappe import _
from bs4 import BeautifulSoup
from pyPdf import PdfFileWriter, PdfFileReader
from frappe.utils import get_site_name
#from frappe.utils.pdf import get_pdf

var = {}
configuracion_hoja = {}

def execute(filters=None):
    columns = [("Producto")+"::300",("Total")+"::80",("Imprimir")+"::60"]
    product_list = traer_pro(filters)
    data = []
    for p_l in product_list:
    	aux = p_l.item_code.encode('utf-8').strip()
        code="'"+p_l.item_code+"'"
        code_id_cbox="'cb"+p_l.item_code+"'"
        code_id_tarea="'ta"+p_l.item_code+"'"
        total=str(p_l.total)
        check="<input onclick=eventCheckbox("+code+") type='checkbox' id="+code_id_cbox+">"
        total_stock = "<textarea id="+code_id_tarea+" onchange=eventTextArea("+code+") rows='1' cols='50'>"+total+"</textarea>"
        row = [ p_l.item_name,total_stock,check]
        data.append(row)
    return columns, data

def get_conditions(filters):
	conditions=""
	if filters:
		if filters.get("item_code"): conditions += filters.get("item_code")

	return conditions

def traer_pro(filters):
	conditions = get_conditions(filters)
	cons = ""
	if conditions:
		cons = frappe.db.sql(""" SELECT total,item_name,item_code
						FROM `tabItem`
                        where `tabItem`.item_code= %s""",conditions, as_dict=1)

	else:
		cons = frappe.db.sql(""" SELECT total,item_name, item_code
						FROM `tabItem`""", as_dict=1)

        return cons
# crear ticket

"""@frappe.whitelist()
def creahtmlprue(name1):
    columns = execute(None)
    print "++++++++++++++Datos", columns
    if name1 != "undefined":
        html = print_ticket(name1)
        name="ticket"
        frappe.local.response.filename = "{name}.pdf".format(name=name.replace(" ", "-").replace("/", "-"))
        frappe.local.response.filecontent = get_pdf(html)
        frappe.local.response.type = "download"
"""
@frappe.whitelist()
def testCallMethod(tickets, config):
    global var
    global configuracion_hoja
    configuracion_hoja = json.loads(config)
    jsonObject = json.loads(tickets)
    var = jsonObject

@frappe.whitelist()
def creahtmlprue():
    html = print_ticket(var)
    name="ticket"
    frappe.local.response.filename = "{name}.pdf".format(name=name.replace(" ", "-").replace("/", "-"))
    frappe.local.response.filecontent = get_pdf(html)
    frappe.local.response.type = "download"

def print_ticket(var):    
    product=traer_pro1(var)
    company=frappe.db.get_value("Company",{},["name"])
    etiqueta=frappe.db.get_value("Stock Settings",{},["mostrar_en_etiqueta_1","mostrar_en_etiqueta_2","mostrar_en_etiqueta_3","mostrar_en_etiqueta_4","leyenda_etiqueta_1","leyenda_etiqueta_2","leyenda_etiqueta_3","leyenda_etiqueta_4"],as_dict=1)
    com='"';
    precio_a=com+"no_add"+com
    precio_b=com+"no_add"+com
    precio_c=com+"no_add"+com
    precio_d=com+"no_add"+com
    etiqueta_1=com+""+com
    etiqueta_2=com+""+com
    etiqueta_3=com+""+com
    etiqueta_4=com+""+com
    if etiqueta.mostrar_en_etiqueta_1 == "1":
        precio_a=com+"agregar"+com
        etiqueta_1=com+etiqueta.leyenda_etiqueta_1+com
    if etiqueta.mostrar_en_etiqueta_2 == "1":
        precio_b=com+"agregar"+com
        etiqueta_2=com+etiqueta.leyenda_etiqueta_2+com
    if etiqueta.mostrar_en_etiqueta_3 == "1":
        precio_c=com+"agregar"+com
        etiqueta_3=com+etiqueta.leyenda_etiqueta_3+com
    if etiqueta.mostrar_en_etiqueta_4 == "1":
        precio_d=com+"agregar"+com
        etiqueta_4=com+etiqueta.leyenda_etiqueta_4+com

    #config: {
    #'etiqueta': 'a10',
    #'pagina': 'a10',
    #precio': false,
    #'nombre_com': true,
    #'bar_code': true
    #}
	for_t1 = ""
    style="<style>p.line {line-height: 100%;font-size:11px;}</style>"    
    inicial_dec = "<div style='text-transform: uppercase; font-size:8px;'>"
    precio_producto = "<br>Precio:"+com+"+product[i].list_price_with_tax+"+com+"<br>"
    nombre_producto = "+product[i].item_name.substr(0,21)+"+com+"<br>"+com+"+product[i].item_name.substr(21,42)+"    
    codigo_producto = "Codigo: "+com+"+product[i].item_code+"
    nombre_compania = "<b>"+company+"</b>"
    final_dec = "</div>"
    inicial_padre = "<div class='padre' style=' display: inline-block;width: auto;margin: 0 0; padding:1px 1px;text-align: justify;'>"
    final_padre = "</div>"

    if configuracion_hoja['pagina'] == 'A10':
        if configuracion_hoja['precio'] == True:
            width = '125px'
            height = '35px'
        else:
            width = "125px"
            height = '45px'
            precio_producto = "<br>"
        imgbarcode = "<img width='"+ width +"' height='"+ height +"' src='https://barcode.tec-it.com/barcode.ashx?data="+com+"+product[i].item_code+"+com+"&code=Code128&dpi=100&dataseparator='/></br>"
        for_t1="for (i in product){ var w=1;while (w <= product[i].cantidad ){document.write("+com+inicial_dec+"<b>"+com+nombre_producto+com+"</b>"+precio_producto+imgbarcode+nombre_compania+final_dec+com+");w++; }}"

    if configuracion_hoja['pagina'] == 'A4':
        nombre_producto = "+product[i].item_name.substr(0,26)+"+com+"<br>"+com+"+product[i].item_name.substr(26,52)+"
        inicial_dec = "<div style='border:1px solid #D3D3D3;text-transform: uppercase; font-size:9px; width:175px; padding:3px 5px 2px 5px;'>"
        if configuracion_hoja['precio'] == True:
            width = '170px'
            height = '50px'
        else:
            width = "170px"
            height = '65px'
            precio_producto = "<br>"
        imgbarcode = "<img width='"+ width +"' height='"+ height +"' src='https://barcode.tec-it.com/barcode.ashx?data="+com+"+product[i].item_code+"+com+"&code=Code128&dpi=100&dataseparator='/></br>"
        for_t1="document.write("+com+"<div style='height: 25px; width:100%'></div>"+com+");for (i in product){ var w=1;while (w <= product[i].cantidad ){document.write("+com+inicial_padre+inicial_dec+"<b>"+com+nombre_producto+com+"</b>"+precio_producto+imgbarcode+nombre_compania+final_dec+final_padre+com+");w++; }}"
    
    
    #for_t1="for (i in product){ var w=1;while (w <= product[i].cantidad ){document.write("+com+"<p class='line' style='font-size:10px'><b><font style='text-transform: uppercase;'>"+com+"+product[i].item_name.substr(0,25)+"+com+"</font></b><br> <b><font style='text-transform: uppercase;'>"+com+"+product[i].item_name.substr(25,50)+"+com+"</b><br>"+company+"<br>"+com+" );document.write("+com+"</br>"+imgbarcode+"</br></br>"+com+");w++; }}"
    #for_t1="for (i in product){ var w=1;while (w <= product[i].qty      ){document.write("+com+"<p class='line'><b><font style='text-transform: uppercase;'>"+com+"+product[i].item_name.substr(0,30)+"+com+"</font></b><br> <b><font style='text-transform: uppercase;'>"+com+"+product[i].item_name.substr(30,60)+"+com+"</font></b><br> CODIGO: "+com+"+product[i].item_code+"+com+"<br>"+com+"+"+com+"</font></b><br> PRECIO: "+com+"+product[i].list_price_with_tax+"+com+"<br>"+com+" );document.write("+com+"<p style='font-size:10px'>"+company+"</br></br></br>"+com+");w++; }}"
    #for_t1="for (i in product){ var w=1;while (w <= product[i].qty      ){document.write("+com+"<p class='line'>"+com+"+product[i].item_code+"+com+"<br>"+com+"+product[i].item_name+"+com+"<br><img src='http://192.168.1.19:8000/files/toners.jpg' alt='' width='60' height='25' align='left'/></br></br></br></br> </p>"+com+" ); w++; }}"
    datos_product = json.dumps(product)    
    path_html ="/home/nodux/nodux-bench/apps/nodux_enterprise_ticket/nodux_enterprise_ticket/print_ticket/report/ticket_por_producto/html.html"
    #path_html ="/home/frappe/frappe-bench/apps/nodux_enterprise_ticket/nodux_enterprise_ticket/print_ticket/report/ticket_por_producto/html.html"
    html=(open(path_html, "rb").read())
    html = html.format(datos_product = datos_product,for_t1=for_t1,style=style,precio_a=precio_a,precio_b=precio_b,precio_c=precio_c,precio_d=precio_d,etiqueta_1=etiqueta_1,etiqueta_2=etiqueta_2,etiqueta_3=etiqueta_3,etiqueta_4=etiqueta_4)
    #print html
    return html

def traer_pro1(var):
    productos = []
    for cod in var:
        #print ">>>>>>>>>> codigo >>>>>", cod['codigo']
        cons = frappe.db.sql(""" SELECT list_price_with_tax, item_code, item_name
            FROM `tabItem`            
            where item_code= %s""", cod['codigo'],as_dict=1)
        #productos.append(cons)
        cons[0]['cantidad']=cod['cantidad']
        productos.append(cons[0])
    print ">>>>>>>>>>>>>>>>", productos
    return productos

#copiado de ticket_producto.py
def get_pdf(html, options=None, output = None):
	html = scrub_urls(html)
	html, options = prepare_options(html, options)
	fname = os.path.join("/tmp", "frappe-pdf-{0}.pdf".format(frappe.generate_hash()))

	try:
		pdfkit.from_string(html, fname, options=options or {})
		if output:
			append_pdf(PdfFileReader(file(fname,"rb")),output)
		else:
			with open(fname, "rb") as fileobj:
				filedata = fileobj.read()

	except IOError as e:
		if ("ContentNotFoundError" in e.message
			or "ContentOperationNotPermittedError" in e.message
			or "UnknownContentError" in e.message
			or "RemoteHostClosedError" in e.message):

			# allow pdfs with missing images if file got created
			if os.path.exists(fname):
				with open(fname, "rb") as fileobj:
					filedata = fileobj.read()

			else:
				frappe.throw(_("PDF generation failed because of broken image links"))
		else:
			raise

	finally:
		cleanup(fname, options)

	if output:
		return output

	return filedata

def append_pdf(input,output):
	# Merging multiple pdf files
    [output.addPage(input.getPage(page_num)) for page_num in range(input.numPages)]

def prepare_options(html, options):
	if not options:
		options = {}
	page_w = '25mm'
	page_h = '38mm'
	margin_l = '0'
	if configuracion_hoja['pagina'] == 'A10':
		page_h = '38mm'
		page_w = '25mm'
		margin_l = '0'
	
	if configuracion_hoja['pagina'] == 'A4':
		page_h = '297mm'
		page_w  = '210mm'
		margin_l = '10mm'

	options.update({
		'print-media-type': None,
		'background': None,
		'images': None,
		'quiet': None,
		'encoding': "UTF-8",
		'margin-left':margin_l,
		'margin-right':'0',
		'margin-top':'0',
		'margin-bottom':'0',
		'orientation' : 'Landscape',
		'page-height':page_h,
		'page-width':page_w,
	})

	html, html_options = read_options_from_html(html)
	options.update(html_options or {})

	# cookies
	if frappe.session and frappe.session.sid:
		options['cookie'] = [('sid', '{0}'.format(frappe.session.sid))]

	return html, options

def read_options_from_html(html):
	options = {}
	soup = BeautifulSoup(html, "html5lib")

	# extract pdfkit options from html
	for html_id in ("margin-top", "margin-bottom", "margin-left", "margin-right", "page-size"):
		try:
			tag = soup.find(id=html_id)
			if tag and tag.contents:
				options[html_id] = tag.contents
		except:
			pass

	options.update(prepare_header_footer(soup))

	toggle_visible_pdf(soup)

	return soup.prettify(), options

def prepare_header_footer(soup):
	options = {}

	head = soup.find("head").contents
	styles = soup.find_all("style")

	bootstrap = frappe.read_file(os.path.join(frappe.local.sites_path, "assets/frappe/css/bootstrap.css"))
	fontawesome = frappe.read_file(os.path.join(frappe.local.sites_path, "assets/frappe/css/font-awesome.css"))

	# extract header and footer
	for html_id in ("header-html", "footer-html"):
		content = soup.find(id=html_id)
		if content:
			# there could be multiple instances of header-html/footer-html
			for tag in soup.find_all(id=html_id):
				tag.extract()

			toggle_visible_pdf(content)
			html = frappe.render_template("templates/print_formats/pdf_header_footer.html", {
				"head": head,
				"styles": styles,
				"content": content,
				"html_id": html_id,
				"bootstrap": bootstrap,
				"fontawesome": fontawesome
			})

			# create temp file
			fname = os.path.join("/tmp", "frappe-pdf-{0}.html".format(frappe.generate_hash()))
			with open(fname, "w") as f:
				f.write(html.encode("utf-8"))

			# {"header-html": "/tmp/frappe-pdf-random.html"}
			options[html_id] = fname

		else:
			if html_id == "header-html":
				options["margin-top"] = "2mm"

			elif html_id == "footer-html":
				options["margin-bottom"] = "0.5mm"

	return options

def cleanup(fname, options):
	if os.path.exists(fname):
		os.remove(fname)

	for key in ("header-html", "footer-html"):
		if options.get(key) and os.path.exists(options[key]):
			os.remove(options[key])

def toggle_visible_pdf(soup):
	for tag in soup.find_all(attrs={"class": "visible-pdf"}):
		# remove visible-pdf class to unhide
		tag.attrs['class'].remove('visible-pdf')

	for tag in soup.find_all(attrs={"class": "hidden-pdf"}):
		# remove tag from html
		tag.extract()

#fin copiado de ticket_producto.py


