// Copyright (c) 2016, ticket and contributors
// For license information, please see license.txt
/* eslint-disable */
var aux = {
  tickets: [],
  config: {
    'etiqueta': 'none',
    'pagina': 'none',
    'precio': true,
    'nombre_com': true,
    'bar_code': true
  }
};
frappe.query_reports["Ticket por Producto"] = {
  "filters": [{
    "fieldname": "item_code",
    "label": __("Codigo Producto"),
    "fieldtype": "Link",
    "options": "Item"
  }],
  onload: function (report) {
    frappe.db.get_value("Print Settings", {}, "tamano_etiqueta", function (r) {
      aux.config.pagina = r.tamano_etiqueta;
    });
    report.page.add_inner_button(__("Generar Tickets PDF"), function () {
      //console.log(aux);
      //var filters = report.get_values();
      /*var w = window.open(
        frappe.urllib.get_full_url("/api/method/nodux_enterprise_ticket.print_ticket.report.ticket_por_producto.ticket_por_producto.creahtmlprue?"
        +"name1="+(filters.item_code)          
        ));*/
      frappe.db.get_value("Print Settings", {}, "tamano_etiqueta", function (r) {
        aux.config.pagina = r.tamano_etiqueta;
      });
      console.log(aux);
      frappe.call({
        method: "nodux_enterprise_ticket.print_ticket.report.ticket_por_producto.ticket_por_producto.testCallMethod",
        args: aux,
        callback: function (r) {
          console.log("callback testcallmethod");
          console.log(r.message);
          //llamar var widow open
          var w = window.open(
            frappe.urllib.get_full_url("/api/method/nodux_enterprise_ticket.print_ticket.report.ticket_por_producto.ticket_por_producto.creahtmlprue"));
        }
      });
    });


    /*
          d.get_input("total").on("change", function () {
            var values = d.get_values();
            d.set_value("cambio", 10);
          });

          d.get_input("pay").on("click", function () {
            console.log("on click");
          });
    */
  }
}

function eventCheckbox(p = '') {
  if (document.getElementById("cb" + p).checked) {
    if (document.getElementById("ta" + p).value < 1) {
      alert("No se aceptan números negativos: " + document.getElementById("ta" + p).value)
      document.getElementById("cb" + p).checked = false;
    } else {
      if (document.getElementById("ta" + p).value > 100) {
        alert("No se aceptan números mayores a 100: " + document.getElementById("ta" + p).value)
        document.getElementById("cb" + p).checked = false;
      } else {
        if (document.getElementById("ta" + p).value % 1 != 0) {
          alert("No se aceptan números decimales: " + document.getElementById("ta" + p).value)
          document.getElementById("cb" + p).checked = false;
        } else {
          aux.tickets.push({
            "codigo": p,
            "cantidad": document.getElementById("ta" + p).value
          });
        }
      }
    }
  } else {
    aux.tickets.forEach(function (obj) {
      //console.log(obj);
      if (obj.codigo == p) {
        //console.log(aux.tickets.indexOf(obj));
        aux.tickets.splice(aux.tickets.indexOf(obj), 1);
      }
    });
  }
  console.log(aux);
}

function eventTextArea(p = '') {
  if (document.getElementById("cb" + p).checked) {
    aux.tickets.forEach(function (obj) {
      if (obj.codigo == p) {
        obj.cantidad = document.getElementById("ta" + p).value;
      }
    });
    console.log(aux);
  }
  //document.getElementById("cb" + p).checked = true;
  //eventCheckbox(p);
}