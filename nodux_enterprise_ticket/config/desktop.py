# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Print Ticket",
			"color": "white",
			"icon": "octicon octicon-file-text",
			"type": "module",
			"label": _("Print Ticket")
		}
	]
