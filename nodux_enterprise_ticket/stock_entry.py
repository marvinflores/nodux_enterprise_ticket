# Copyright (c) 2013, NODUX and contributors
# For license information, please see license.txt
# item-costo-precio de venta-diferencia-utilidad-costo-costo-venta%-venta
from __future__ import unicode_literals
import frappe
import time
from frappe import utils
from frappe.model.document import Document
from frappe.utils import flt
from frappe import msgprint, _
import json
import pdfkit, os, frappe
from frappe.utils import scrub_urls
from frappe import _
from bs4 import BeautifulSoup
from pyPdf import PdfFileWriter, PdfFileReader
from frappe.utils import get_site_name

@frappe.whitelist()
def creahtml_se(name):
    if name != "undefined":
        html = print_ticket(name)
        name="ticket"
        frappe.local.response.filename = "{name}.pdf".format(name=name.replace(" ", "-").replace("/", "-"))
        frappe.local.response.filecontent = get_pdf(html)
        frappe.local.response.type = "download"

def print_ticket(item_code):
    product=get_item(item_code)
    company=frappe.db.get_value("Company",{},["name"])
    etiqueta=frappe.db.get_value("Stock Settings",{},["mostrar_en_etiqueta_1","mostrar_en_etiqueta_2","mostrar_en_etiqueta_3","mostrar_en_etiqueta_4","leyenda_etiqueta_1","leyenda_etiqueta_2","leyenda_etiqueta_3","leyenda_etiqueta_4"],as_dict=1)
    add_quotes='"';
    precio_a=add_quotes+"no_add"+add_quotes
    precio_b=add_quotes+"no_add"+add_quotes
    precio_c=add_quotes+"no_add"+add_quotes
    precio_d=add_quotes+"no_add"+add_quotes
    etiqueta_1=add_quotes+""+add_quotes
    etiqueta_2=add_quotes+""+add_quotes
    etiqueta_3=add_quotes+""+add_quotes
    etiqueta_4=add_quotes+""+add_quotes
    if etiqueta.mostrar_en_etiqueta_1 == "1":
        precio_a=add_quotes+"agregar"+add_quotes
        if etiqueta.leyenda_etiqueta_1:
            etiqueta_1=add_quotes+etiqueta.leyenda_etiqueta_1+add_quotes
    if etiqueta.mostrar_en_etiqueta_2 == "1":
        precio_b=add_quotes+"agregar"+add_quotes
        if etiqueta.leyenda_etiqueta_2:
            etiqueta_2=add_quotes+etiqueta.leyenda_etiqueta_2+add_quotes
    if etiqueta.mostrar_en_etiqueta_3 == "1":
        precio_c=add_quotes+"agregar"+add_quotes
        if etiqueta.leyenda_etiqueta_3:
            etiqueta_3=add_quotes+etiqueta.leyenda_etiqueta_3+add_quotes
    if etiqueta.mostrar_en_etiqueta_4 == "1":
        precio_d=add_quotes+"agregar"+add_quotes
        if etiqueta.leyenda_etiqueta_4:
            etiqueta_4=add_quotes+etiqueta.leyenda_etiqueta_4+add_quotes

    style="<style>p.line {line-height: 100%;font-size:12px;}</style>"
    for_t1="for (i in product){ var w=1;while (w <= 1){document.write("+add_quotes+"<p class='line'><b><font style='text-transform: uppercase;'>"+add_quotes+"+product[i].item_name.substr(0,30)+"+add_quotes+"</font></b><br> <b><font style='text-transform: uppercase;'>"+add_quotes+"+product[i].item_name.substr(31,60)+"+add_quotes+"</font></b><br> <b>CODIGO: "+add_quotes+"+product[i].item_code+"+add_quotes+"</b><br>"+add_quotes+" ); if (precio_a == "+add_quotes+"agregar"+add_quotes+"){document.write(etiqueta_1+"+add_quotes+": "+add_quotes+"+product[i].precio_a+"+add_quotes+"<br>"+add_quotes+");}if (precio_b == "+add_quotes+"agregar"+add_quotes+"){document.write(etiqueta_2+"+add_quotes+": "+add_quotes+"+product[i].precio_b+"+add_quotes+"<br>"+add_quotes+");}if (precio_c== "+add_quotes+"agregar"+add_quotes+"){document.write(etiqueta_3+"+add_quotes+": "+add_quotes+"+product[i].precio_c+"+add_quotes+"<br>"+add_quotes+");}if (precio_d == "+add_quotes+"agregar"+add_quotes+"){document.write(etiqueta_4+"+add_quotes+": "+add_quotes+"+product[i].precio_d);}document.write("+add_quotes+"</br></br><p style='font-size:6px'> EMPRESA TECNOLOGICA TONERS CIA. LTDA.<img src='http://192.168.1.19:8000/files/toners.jpg' alt='' width='35' height='20' align='left'/></br></br></br></br>"+add_quotes+");w++; }}"
    datos_product = json.dumps(product)

    path_html ="/home/frappe/frappe-bench/apps/nodux_enterprise_ticket/nodux_enterprise_ticket/print_ticket/report/productos_compra/html.html"
    html=(open(path_html, "rb").read())
    html = html.format(datos_product=datos_product,for_t1=for_t1,style=style,precio_a=precio_a,precio_b=precio_b,precio_c=precio_c,precio_d=precio_d,etiqueta_1=etiqueta_1,etiqueta_2=etiqueta_2,etiqueta_3=etiqueta_3,etiqueta_4=etiqueta_4)
    print html
    return html

def get_item(item_code):
    cons = frappe.db.sql(""" SELECT item_name, item_code, precio_a, precio_b, precio_c, precio_b, precio_a_con_impuestos, precio_b_con_impuestos, precio_c_con_impuestos, precio_d_con_impuestos FROM `tabItem`
                    where item_code = %s""",item_code,as_dict=1)
    return cons

def get_pdf(html, options=None, output = None):
	html = scrub_urls(html)
	html, options = prepare_options(html, options)
	fname = os.path.join("/tmp", "frappe-pdf-{0}.pdf".format(frappe.generate_hash()))

	try:
		pdfkit.from_string(html, fname, options=options or {})
		if output:
			append_pdf(PdfFileReader(file(fname,"rb")),output)
		else:
			with open(fname, "rb") as fileobj:
				filedata = fileobj.read()

	except IOError as e:
		if ("ContentNotFoundError" in e.message
			or "ContentOperationNotPermittedError" in e.message
			or "UnknownContentError" in e.message
			or "RemoteHostClosedError" in e.message):

			# allow pdfs with missing images if file got created
			if os.path.exists(fname):
				with open(fname, "rb") as fileobj:
					filedata = fileobj.read()

			else:
				frappe.throw(_("PDF generation failed because of broken image links"))
		else:
			raise

	finally:
		cleanup(fname, options)

	if output:
		return output

	return filedata

def append_pdf(input,output):
	# Merging multiple pdf files
    [output.addPage(input.getPage(page_num)) for page_num in range(input.numPages)]

def prepare_options(html, options):
	if not options:
		options = {}

	options.update({
		'print-media-type': None,
		'background': None,
		'images': None,
		'quiet': None,
		'encoding': "UTF-8",
		'margin-right': '0.4mm',
		'margin-left': '0.4mm',
        'orientation' : 'landscape',
        'page-size':'A9'

	})

	html, html_options = read_options_from_html(html)
	options.update(html_options or {})

	# cookies
	if frappe.session and frappe.session.sid:
		options['cookie'] = [('sid', '{0}'.format(frappe.session.sid))]

	return html, options

def read_options_from_html(html):
	options = {}
	soup = BeautifulSoup(html, "html5lib")

	# extract pdfkit options from html
	for html_id in ("margin-top", "margin-bottom", "margin-left", "margin-right", "page-size"):
		try:
			tag = soup.find(id=html_id)
			if tag and tag.contents:
				options[html_id] = tag.contents
		except:
			pass

	options.update(prepare_header_footer(soup))

	toggle_visible_pdf(soup)

	return soup.prettify(), options

def prepare_header_footer(soup):
	options = {}

	head = soup.find("head").contents
	styles = soup.find_all("style")

	bootstrap = frappe.read_file(os.path.join(frappe.local.sites_path, "assets/frappe/css/bootstrap.css"))
	fontawesome = frappe.read_file(os.path.join(frappe.local.sites_path, "assets/frappe/css/font-awesome.css"))

	# extract header and footer
	for html_id in ("header-html", "footer-html"):
		content = soup.find(id=html_id)
		if content:
			# there could be multiple instances of header-html/footer-html
			for tag in soup.find_all(id=html_id):
				tag.extract()

			toggle_visible_pdf(content)
			html = frappe.render_template("templates/print_formats/pdf_header_footer.html", {
				"head": head,
				"styles": styles,
				"content": content,
				"html_id": html_id,
				"bootstrap": bootstrap,
				"fontawesome": fontawesome
			})

			# create temp file
			fname = os.path.join("/tmp", "frappe-pdf-{0}.html".format(frappe.generate_hash()))
			with open(fname, "w") as f:
				f.write(html.encode("utf-8"))

			# {"header-html": "/tmp/frappe-pdf-random.html"}
			options[html_id] = fname

		else:
			if html_id == "header-html":
				options["margin-top"] = "2mm"

			elif html_id == "footer-html":
				options["margin-bottom"] = "0.5mm"

	return options

def cleanup(fname, options):
	if os.path.exists(fname):
		os.remove(fname)

	for key in ("header-html", "footer-html"):
		if options.get(key) and os.path.exists(options[key]):
			os.remove(options[key])

def toggle_visible_pdf(soup):
	for tag in soup.find_all(attrs={"class": "visible-pdf"}):
		# remove visible-pdf class to unhide
		tag.attrs['class'].remove('visible-pdf')

	for tag in soup.find_all(attrs={"class": "hidden-pdf"}):
		# remove tag from html
		tag.extract()
