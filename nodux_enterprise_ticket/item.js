frappe.provide("erpnext.item");

frappe.ui.form.on("Item", {

  refresh: function(frm) {
    frm.add_custom_button(__("Generar Ticket"), function() {
      var w = window.open(
        frappe.urllib.get_full_url("/api/method/nodux_enterprise_ticket.item.creahtml?"
        +"item_code="+(frm.doc.item_code)
        ));
    }).addClass("btn-primary");
	}
});
